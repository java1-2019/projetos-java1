/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import javax.swing.JOptionPane;
import model.Aluno;
import model.Funcionario;
/**
 *
 * @author GABRIEL HENKE
 */
public class Controller {
    
    public void cadastroAluno(String nome, String cpf, String rg, String idade, int numMatricula, int termo, double nota){
        Aluno a = new Aluno(nome, cpf, rg, idade, numMatricula, termo, nota);
        a.inserirAluno();
        JOptionPane.showMessageDialog(null, "Aluno cadastrado!");
    }
    
    public void cadastroFuncionario(String nome, String cpf, String rg, String idade, double salario, String departamento) {
        Funcionario f = new Funcionario(nome, cpf, rg, idade, salario, departamento);
        f.inserirFuncionario();
        JOptionPane.showMessageDialog(null, "Funcionário cadastrado!");
    } 
}
