/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author GABRIEL HENKE
 */
public class Funcionario extends Pessoa implements FuncionarioActions {
    double salario;
    String departamento;
    
    public Funcionario (String nome, String cpf, String rg, String idade, double salario, String departamento) {
        super(nome, cpf, rg, idade);
        this.salario = salario;
        this.departamento = departamento;
    }
    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
    @Override
    public void aumentaSalario(double valor) {
        this.salario+=valor;
    }
    
    @Override
    public void registraPonto() {
        System.out.println("bateu ponto");
    }
    
    public void inserirFuncionario() {
        //query de insertar no banco
    }
    
    @Override
    public void listarPessoas() {
        System.out.println("Listando pessoas pelo SQL");
    }
}
