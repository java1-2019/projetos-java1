/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author GABRIEL HENKE
 */
public class Aluno extends Pessoa {
    private int numMatricula;
    private int termo;
    private double nota;

    public Aluno(String nome, String cpf, String rg, String idade, int numMatricula, int termo, double nota) {
        super(nome, cpf, rg, idade);
        this.numMatricula = numMatricula;
        this.termo = termo;
        this.nota = nota;
    }
    public double getNota() {
        return nota;
    }
    
    public void setNota(double nota) {
        this.nota = nota;
    }

    public int getNumMatricula() {
        return numMatricula;
    }

    public void setNumMatricula(int numMatricula) {
        this.numMatricula = numMatricula;
    }

    public int getTermo() {
        return termo;
    }

    public void setTermo(int termo) {
        this.termo = termo;
    }
    
    public void inserirAluno(){
        
    }
    
    @Override
    public void listarPessoas() {
        System.out.println("SQL list");
    }
}
