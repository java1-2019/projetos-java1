/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Pessoa {
    private String nome;
    private int idade;
    private double altura;
    private double peso;
    private String cpf;
    
    public String getNome() {
        return this.nome;
    }
    
    public int getIdade() {
        return this.idade;
    }
    
    public double getAltura() {
        return this.altura;
    }
    
    public double getPeso() {
        return this.peso;
    }
    
    public String getCpf() {
        return this.cpf;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    public void setPeso(double peso) {
        this.peso = peso;
    }
    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public double calcularIMC() {
        return this.peso/(this.altura*this.altura);
    }
    
    public double calcularIMC(double peso, double altura) {
        return peso/(altura*altura);
    }
    
    public void classificarIMC(double imc) {
        
        if(imc<16) {
            System.out.println("Magreza Grave!");
        } else {
            if (imc>16&&imc<17){
                System.out.println("Magreza moderada!");
            } else {
                if (imc>17 && imc< 18) {
                    System.out.println("Magreza Leve");
                } else { 
                    if (imc>18.8 && imc < 25){
                        System.out.println("Saudável");
                    } else {
                        if (imc>25 && imc<30){
                            System.out.println("Sobrepeso");
                        } else {
                            if (imc>30 && imc<35) {
                                System.out.println("Obesidade grau I");
                            } else {
                                if (imc>35 && imc<40){
                                    System.out.println("Obesidade Grau II");
                                } else {
                                    if (imc>=40) {
                                        System.out.println("Obesidade Grau III (Mórbida)");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void exibir() {
        System.out.println("Nome:"+ getNome());
        System.out.println("Idade: "+ getIdade());
        System.out.println("Altura: "+ getAltura());
        System.out.println("Peso: "+ getPeso());
        System.out.println("Cpf: "+ getCpf());
        System.out.println("IMC: "+ calcularIMC());
        classificarIMC(calcularIMC());    }
}