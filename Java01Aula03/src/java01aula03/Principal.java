/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java01aula03;

import javax.swing.JOptionPane;

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Funcionario f = new Funcionario ("Henke");
        
        f.salario = 10000;
  
        
        JOptionPane.showMessageDialog(null, "Nome: " + f.nome + "\nSalario: "+ f.salario);
        f.aumentarSalario(0.5);
        JOptionPane.showMessageDialog(null, "Salario Aumentado: "+ f.salario);
        f.aumentarSalario();
        
        JOptionPane.showMessageDialog(null, "Juros Compostos: "+ f.calculaJurosComposto(10000, 20, 2) + "\nSalário novo: "+ f.salario);
    }
    
}
