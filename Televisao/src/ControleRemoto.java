/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class ControleRemoto {
    Televisao tv;
    
    public ControleRemoto(Televisao tv){
        this.tv = tv;
    }
    
    public void aumentarVolume() {
        this.tv.setVolumeAtual(this.tv.getVolumeAtual() + 1);
    }
    
    public void diminuiVolume(){
        this.tv.setVolumeAtual(this.tv.getVolumeAtual() - 1);
    }
    
    public void sobeCanal() {
        this.tv.setCanalAtual(this.tv.getCanalAtual() + 1);
    }
    
    public void desceCanal() {
        this.tv.setCanalAtual(this.tv.getCanalAtual() - 1);
    }
    
    public void trocarCanalDireto(int canal) {
        this.tv.setCanalAtual(canal);
    }
    
    public int consultaCanalAtual(){
        return tv.getCanalAtual();
    }
}
