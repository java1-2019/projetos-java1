/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author GABRIEL HENKE
 */
public class CadFarmacia {
    public static void main(String args[]) {
        JFrame pharmacy = new JFrame("Farmacia");
        pharmacy.setSize(600, 350);
        pharmacy.setVisible(true);
        pharmacy.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel panel = new JPanel();
        
        JButton bt1 = new JButton("Neosoro");
        JButton bt2 = new JButton("Novalgina");
        JButton bt3 = new JButton("Tilenol");
        JButton bt4 = new JButton("Rivotril");
        JButton bt5 = new JButton("Omeoprazol");
        
        panel.setLayout(new FlowLayout());
        
        panel.add(bt1);
        panel.add(bt2);
        panel.add(bt3);
        panel.add(bt4);
        panel.add(bt5);
        
       
        pharmacy.setContentPane(panel);
        
    }
}
