/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author GABRIEL HENKE
 */
public class CadCliente {
        public static void main(String[] args) {
        JFrame x = new JFrame("Teste Aula Swing");
        x.setSize(600,350);
        x.setVisible(true);
        x.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel jp = new JPanel();
        
        x.setContentPane(jp);
        
        JLabel rotulo = new JLabel();
        rotulo.setText("Nome: ");
        jp.add(rotulo);
        
        JTextField jtfNome = new JTextField(50);
        jp.add(jtfNome);
        
        JTextArea jta = new JTextArea(10,50);
        jp.add(jta);
        
        JButton bt = new JButton("VITIMBROXA");
        JPasswordField pw = new JPasswordField(50);
        jp.add(bt);
        jp.add(pw);
        
        JButton bt1 = new JButton("Sim");
        jp.add(bt1);
        
        JCheckBox ckbx = new JCheckBox("Java");
        JCheckBox ckbx2 = new JCheckBox("Python");
        JCheckBox ckbx3= new JCheckBox("Cobol");
        JCheckBox ckbx4 = new JCheckBox("Fortran");
        
        jp.add(ckbx);
        jp.add(ckbx2);
        jp.add(ckbx3);
        jp.add(ckbx4);
        
 
        
        String cidades[] = new String[6];
        cidades[0] = "Atlanta";
        cidades[1] = "Mexico";
        cidades[2] = "Totenhanm";
        cidades[3] = "Barcelona";
        cidades[4] = "Tupã";
        cidades[5] = "Cidade Meczada";
        
        JComboBox cbCidade = new JComboBox(cidades);
        
        jp.add(cbCidade);
        x.setContentPane(jp);
    }
}
