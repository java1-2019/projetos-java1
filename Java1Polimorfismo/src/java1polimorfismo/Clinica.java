/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java1polimorfismo;

/**
 *
 * @author GABRIEL HENKE
 */
public class Clinica {
    private String nome;
    private String veterinario;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(String veterinario) {
        this.veterinario = veterinario;
    }
    
    public void fazerCirurgia(Mamifero animal) {
        System.out.println(" Operado");
    }
}
