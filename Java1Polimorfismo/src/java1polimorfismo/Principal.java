/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java1polimorfismo;

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Clinica cli = new Clinica();
        
        Gato tom = new Gato();
        Cachorro rex = new Cachorro();
        
        cli.fazerCirurgia(tom);
        cli.fazerCirurgia(rex);
    }
    
}
