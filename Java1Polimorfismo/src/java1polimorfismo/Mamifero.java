/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java1polimorfismo;

/**
 *
 * @author GABRIEL HENKE
 */
public class Mamifero {
    
    private int numGlandulasMamarias;
    private int numDentes;

    public int getNumGlandulasMamarias() {
        return numGlandulasMamarias;
    }

    public void setNumGlandulasMamarias(int numGlandulasMamarias) {
        this.numGlandulasMamarias = numGlandulasMamarias;
    }

    public int getNumDentes() {
        return numDentes;
    }

    public void setNumDentes(int numDentes) {
        this.numDentes = numDentes;
    }
    
}
