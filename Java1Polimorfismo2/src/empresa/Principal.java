package empresa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Relogio r = new Relogio();
        Diretor d = new Diretor("Adriano");
        Gerente g = new Gerente("JEfferson");
        Programador p = new Programador("Joao");
        
        
        r.baterPonto(d);
        r.baterPonto(p);
        r.baterPonto(g);
    }
    
}
