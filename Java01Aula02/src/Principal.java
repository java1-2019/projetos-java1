
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Carro c1 = new Carro (); //INSTANCIAÇÃO
        
        c1.andar();
        c1.modelo = "etios";
        c1.marca = "Toyota";
        c1.preco = 54554.88;
        c1.cor = "Preto";
        c1.ano = "2014";
        c1.frear();
        c1.exibir();
        
        Pessoa p = new Pessoa ();
        
        p.nome = "Raisler";
        p.idade = 18;
        p.peso = 50;
        
        c1.prop = p;
        
        System.out.println (c1.prop.nome);
    }
    
}
