
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Carro {
    public String cor;
    public double preco;
    public String marca;
    public String modelo;
    public String ano;
    public Pessoa prop; // RELACIONAMENTO
    
    public void ligar(){
        JOptionPane.showMessageDialog(null,"O carro está ligado");
    }
    
    public void andar (){
        JOptionPane.showMessageDialog(null, "O carro está andando");    
    }
    
    public void frear (){
        JOptionPane.showMessageDialog(null, "O carro está frea...");
    }
    public void exibir(){
        JOptionPane.showMessageDialog(null, "Marca: "+ this.marca + "\nModelo: "+this.modelo+"\nAno: " + this.ano +"\nCor: "+ this.cor+"\nPreço: " + this.preco +"\n"+ prop);
    }
}
