/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java1revp1;

/**
 *
 * @author GABRIEL HENKE
 */
public class Carro extends Terrestre{
    private String marca;
    private String modelo;
    private String cor;
    private Motor motor;
    
    public void ligar() {
        motor.setStatus("ON");
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }
    
    public void andar() {
        System.out.println("Andando....");
    }
    
    public void desligar() {
        motor.setStatus("OFF");
    }
}
