/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java1revp1;

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Motor mGol = new Motor();
        mGol.setMarca("Volkswagem");
        mGol.setModelo("EA-211");
        Motor.setPotencia(104);
        mGol.setTorque(66);
        Motor.setPotencia(120);
        Motor mFerrari = new Motor();
        
        Carro gol = new Carro();
        gol.setMarca ("Volkswagen");
        gol.setModelo("GOL MSI");
        gol.setCor("Vermelho");
        gol.setMotor(mGol);
        
        Carro ferrari = new Carro();
        ferrari.setMarca("Ferrari");
        ferrari.setModelo("Spider");
        ferrari.setCor("Bege");
        ferrari.setMotor(mFerrari);
        ferrari.setAroRoda(20);
        
        gol.ligar();
        gol.andar();
        System.out.println("Motor: "+gol.getMotor().getStatus());
        gol.desligar();
        System.out.println("Motor: "+gol.getMotor().getStatus());
        System.out.println("Potencia: "+Motor.getPotencia());
        System.out.println("Força Disp: "+gol.getMotor().forcaDisponivel(5400));
    }
    
}
