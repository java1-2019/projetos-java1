/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java1revp1;

/**
 *
 * @author GABRIEL HENKE
 */
public class Motor {
    private String marca;
    private String modelo;
    private static int potencia;
    private double torque;
    private String status = "OFF";

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public static int getPotencia() {
        return potencia;
    }

    public static void setPotencia(int potencia) {
        Motor.potencia = potencia;
    }

    public double getTorque() {
        return torque;
    }

    public void setTorque(double torque) {
        this.torque = torque;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    double forcaDisponivel(int rpm) {
        return this.torque / rpm;
    }
    
    public String getMarca() {
        return this.marca;
    }
    
    public void setMarca(String marca) { 
        this.marca = marca;
    }
}
