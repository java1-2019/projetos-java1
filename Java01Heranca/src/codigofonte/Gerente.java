/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigofonte;

/**
 *
 * @author GABRIEL HENKE
 */
public class Gerente extends Funcionario {
    
    private String cargo = "Gerente";
    private String setor;
    
    public Gerente() {
        System.out.println("Construir gerente!");
    }
    
    public String getSetor() {
        return this.setor;
    }
    
    public void setSetor(String setor) {
        this.setor = setor;
    }
    
    /**
     *
     */
    @Override
    public void aumentaSalario() {
        this.setSalario(this.getSalario()+540);
    }
}
