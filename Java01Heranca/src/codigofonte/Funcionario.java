/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigofonte;

/**
 *
 * @author GABRIEL HENKE
 */
public class Funcionario extends Pessoa {
    
    private String cargo;
    private double salario;
    
    public Funcionario() {
        System.out.println("Constrir funcionario");
    }
    public String getCargo() {
        return this.cargo;
    }
    
    public double getSalario() {
        return this.salario;
    }
    
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    @Override
    public void andar() {
        System.out.println("Funcionario anda");
    }
    
    public void aumentaSalario() {
        this.salario += 100;
    }
}
