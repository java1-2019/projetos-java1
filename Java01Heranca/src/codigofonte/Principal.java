package codigofonte;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        
//        Funcionario f = new Funcionario();;
//        
//        f.setIdade(43);
//        f.setSalario(10.5000);
//        
//        Pessoa p = new Pessoa();
//        p.setIdade(33);
//        
//        Cliente c = new Cliente();
//        c.andar();
//        f.andar();
//        
//        Gerente g = new Gerente();
//        g.setSetor("Marketing");
        
        Funcionario f = new Funcionario();
        Gerente g = new Gerente();
        Diretor d = new Diretor();
        
        f.setSalario(950);
        g.setSalario(5000);
        d.setSalario(10000);
        
        System.out.println("==== SALARIOS ===");
        System.out.println("Funcioario: "+ f.getSalario());
        System.out.println("Gerente: "+ g.getSalario());
        System.out.println("Diretor: "+d.getSalario());
        
        System.out.println("\nAPOS O AUMENTO:");
        
        f.aumentaSalario();
        g.aumentaSalario();
        d.aumentaSalario();
        
        System.out.println("Funcioario: "+ f.getSalario());
        System.out.println("Gerente: "+ g.getSalario());
        System.out.println("Diretor: "+d.getSalario());
    }
    
}
