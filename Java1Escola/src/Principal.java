/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic her
        
        Curso c1 = new Curso ();
        c1.nome = "Ciencia da Computação";
        c1.professor = "Favan";
        c1.turno = "Tarde";
        
        Disciplina d1 = new Disciplina ();
        d1.nome = "Projeto e Análise de Algoritmo";
        d1.id = 1;
        d1.curso = c1;
        
        Aluno a1 = new Aluno ();
        a1.nome = "Godofredo";
        a1.idade = 88;
        a1.altura = 1.45;
        a1.curso = c1;
        
        System.out.println ("Nome do aluno: "+ a1.nome);
        System.out.println ("Curso: "+ a1.curso.nome);
    }
    
}
