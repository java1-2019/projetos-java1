/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import java.util.Date;

/**
 *
 * @author GABRIEL HENKE
 */
public class Conta {
    private double saldo;
    private Date dataAbertura;

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    /*
    * Método para depositar um valor no saldo da conta.
    */
    public void deposita(double valor) {
        if(valor >= 0) {
            this.saldo += valor;
        }
    }
}
