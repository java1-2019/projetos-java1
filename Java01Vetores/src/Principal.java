
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//        int [] numeros = new int[100];;
//        
//        numeros[0] = 136;
//        numeros[9] = 17;
//        
//        System.out.println(numeros[0]);
//        System.out.println(numeros[9]);
//        System.out.println(numeros[4]);
//////////////////////////////////////////////////////////////////////////////
//        int[] num = new int [100];
//        
//        for(int i=0; i<100; i++){
//            num[i] = i;
//            System.out.println("num["+i+"] = "+ num[i]);
//        }
//////////////////////////////////////////////////////////////////////////////
//        
//          String[] nomes = new String[] {
//              "Raisler", "Willy", "Gandalf", "Marcao"
//          };
//          
//          Arrays.sort(nomes);
//          
//          for (String nome : nomes) {
//              System.out.println(nome);
//          }
//          
//          String[] nomes2 = Arrays.copyOf(nomes, 2); //VETORES COM INDICES DIFERENTES NÃO FUNCIONA
//          for (String nome : nomes2) {
//              System.out.println(nome);
//          }
//          
//          int[] numeros = new int[10];
//          
//          java.util.Arrays.fill(numeros, 7);
//          
//          for (int numero : numeros){
//              System.out.println(numero);
//          }
////////////////////////////////////////////////////////////////////////////
            
        Pessoa[] alunos = new Pessoa[10];

        alunos[0].nome = "Jair";
        alunos[0].idade = 34;

        alunos[1].nome = "Dilma";
        alunos[1].idade = 65;

        alunos[2].nome = "Haddad";
        alunos[2].idade = 23;
    }   
}
