/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente();
        ContaPoupanca cp = new ContaPoupanca();
        
        cc.setSaldo(0);
        cp.setSaldo(0);
        
        cc.deposita(343434);
        cp.deposita(10000);

        
        cc.imprimeExtrato();
        cp.imprimeExtrato();
    }
    
}
