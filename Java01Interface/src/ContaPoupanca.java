/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class ContaPoupanca implements Conta, CaixaEletronico {
    private String titular;
    private String aniversario;
    private int numConta;
    private int agencia;
    private double saldo;

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getAniversario() {
        return aniversario;
    }

    public void setAniversario(String aniversario) {
        this.aniversario = aniversario;
    }

    public int getNumConta() {
        return numConta;
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    @Override
    public void saca(double valor) {
        if (valor <= this.saldo) {
            this.saldo -= valor;
        } else {
            System.out.println("Saldo insuficiente!");
        }
    }
    
    @Override
    public void deposita(double valor) {
        this.saldo += saldo;
    }
    
    public void render() {
        this.saldo *= 1.0025;
    }
    
    @Override
    public void imprimeExtrato() {
        System.out.println("Tipo: POUPANÇA");
        System.out.println("Titular: "+ this.titular);
        System.out.println("Aniversário: "+ this.aniversario);
        System.out.println("Conta: "+ this.numConta);
        System.out.println("Agência: "+ this.agencia);
        System.out.println("Saldo: "+ this.saldo);
        System.out.println("===============================");
    }
}
