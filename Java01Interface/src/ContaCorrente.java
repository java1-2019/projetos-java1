/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class ContaCorrente implements Conta, CaixaEletronico {
    private  String titular;
    private int agencia;
    private int numConta;
    private double saldo;
    private double limite;

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumConta() {
        return numConta;
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }
    
    @Override
    public void deposita(double valor) {
        this.saldo += valor;
    }
    
    @Override
    public void saca(double valor) {
        if (valor > this.saldo + this.limite) {
            System.out.println("Saldo insuficiente!");
        } else
            if (valor > this.saldo) {
                valor -= this.saldo;
                this.saldo = 0;
                this.limite -= valor;
            }
        this.saldo -= valor;
    }
    
    @Override
   public void imprimeExtrato() {
       System.out.println("Tipo: CONTA CORRENTE");
       System.out.println("Titular: "+ this.titular);
       System.out.println("Agência: "+ this.agencia);
       System.out.println("Conta: "+ this.numConta);
       System.out.println("Saldo: "+ this.saldo);
       System.out.println("Limite Atual: "+ this.limite);
       System.out.println("===============================");
   }
}
