/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Conta {
    private double saldo;

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    public void deposita(double valor) throws Exception {
        if (valor <= 0) {
            Exception erro = new Exception();
            throw erro;
        } else {
            this.saldo += valor;
            System.out.println("Depositado "+valor+" reais");
        }
    }
}
