/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GABRIEL HENKE
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Conta caraio = new Conta();
        
        try {
            caraio.deposita(-2000); //depositando um valor menor que 0 para acionar o exception
        } catch (Exception e) {
            System.out.println("Erro: "+ e.getCause());
        }
    }
    
}
